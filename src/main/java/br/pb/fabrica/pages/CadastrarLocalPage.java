package br.pb.fabrica.pages;

import br.pb.fabrica.core.BasePage;
import org.openqa.selenium.By;

public class CadastrarLocalPage extends BasePage {

    public void setNomeDoLocal(String texto){
        escrever("id_nome", texto);
    }


    /* Botões */

    public void clicarBotaoEnviar(){
        clicarBotao("//button[text()='Enviar']");
    }

}
