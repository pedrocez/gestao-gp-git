package br.pb.fabrica.testes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.pb.fabrica.core.BaseTest;
import br.pb.fabrica.pages.cadastrarEstagioPage;
/*
 *  Analista de Testes: Pedro Carvalho 
 *  Data: 13/10/2020
 *  
 */

public class CadastrarEstagio extends BaseTest{
		cadastrarEstagioPage cadastro = new cadastrarEstagioPage();
	@Test
	public void DeveCadastrarEstagio() throws InterruptedException {
		cadastro.clicarBotaoCadastro();
		cadastro.AcessarTelaCadastro();
		cadastro.setConcedente("Ortotrauma");
		cadastro.setCurso("Medicina");
		cadastro.setDisciplina("Cir�rgico");
		cadastro.setTurno("Integral");
		cadastro.setQuantidadeDeAlunos("30");
		cadastro.setCustoPorAluno("R$5000.00");
		cadastro.setPreceptor("Wallace");
		cadastro.setLocal("Hospital de Trauma");
		cadastro.setTipoDeEstabelecimento("Unidade de sa�de");
		cadastro.setSetor("Bloco cir�rgico");
		cadastro.setData("11/10/2020");
		cadastro.clicarBotaoEnviar();
		
		assertEquals("Ortotrauma", cadastro.obterTextoTipoConvenio());
		
		}
}
